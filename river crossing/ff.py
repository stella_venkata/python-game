import pygame, os, pygame.mixer, sys
from pygame.locals import *
from config import *
script_dir = os.path.dirname(__file__)
rel_path = "fold/images/"
abs_file_path = os.path.join(script_dir, rel_path)

pygame.init()
#sound = pygame.mixer.Sound('start.wav')
#laser = pygame.mixer.Sound('laser.wav')
#sound.play()
display_width = 1200
display_height = 1000
black = (0,0,0)
white = (255,255,255)
red = (255,0,0)
Blue = (0,0,255)

gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption('River Crossing ships')
clock = pygame.time.Clock() 

leadmg = pygame.image.load(abs_file_path+"lead.gif")
startmg = pygame.image.load(abs_file_path+"start.png")
plaenmg = pygame.image.load(abs_file_path+"part.png")
mullamg = pygame.image.load(abs_file_path+"pirate.png")
ghostmg = pygame.image.load(abs_file_path+"ghost.png")
shipmg = pygame.image.load(abs_file_path + "ship.png")
heromg = pygame.image.load(abs_file_path + "hero.png")
#definiing their size
leadmg = pygame.transform.scale(leadmg,(80,80))
heromg = pygame.transform.scale(heromg,(80,80))
startmg = pygame.transform.scale(startmg,(1200,50))
plaenmg = pygame.transform.scale(plaenmg,(1200,50))
mullamg = pygame.transform.scale(mullamg,(80,75))
ghostmg = pygame.transform.scale(ghostmg,(80,75))
shipmg = pygame.transform.scale(shipmg,(100,100))
#miniaturelead = pygame.transform.scale(lead1,(20,20))
#rectangle border for all images
leadrect = leadmg.get_rect()
startrect = startmg.get_rect()
plaenrect = plaenmg.get_rect()
mullarect = mullamg.get_rect()
mulla2rect = mullamg.get_rect()
ghostrect = ghostmg.get_rect()
shiprect = shipmg.get_rect()
herorect = heromg.get_rect()
#masking tall the images ; useful in  further image collisons
leadmg_mask = pygame.mask.from_surface(leadmg)
mullamg_mask = pygame.mask.from_surface(mullamg)
ghostmg_mask = pygame.mask.from_surface(ghostmg)
shipmg_mask = pygame.mask.from_surface(shipmg)
heromg_mask = pygame.mask.from_surface(heromg)
def start(sx,sy):
    gameDisplay.blit(startmg,(sx,sy))
def lead(x,y):
    gameDisplay.blit(leadmg,(x,y))
def plaen(px,py):
    gameDisplay.blit(plaenmg,(px,py))
def mulla(mx,my):
    gameDisplay.blit(mullamg,(mx,my))
def palen2(px,p2y):
    gameDisplay.blit(plaenmg,(px,p2y))
def palen3(px,p3y):
    gameDisplay.blit(plaenmg,(px,p3y))
def palen4(px,p4y):
    gameDisplay.blit(plaenmg,(px,p4y))
def palend(px,pend):
    gameDisplay.blit(plaenmg,(px,pend))
def mulla2(m2x,m2y):
    gameDisplay.blit(mullamg, (m2x,m2y))
def ghost(g1x,g1y):
    gameDisplay.blit(ghostmg, (g1x,g1y))
def ship(movx,movy):
    gameDisplay.blit(shipmg, (movx,movy))
def hero(hx,hy):
    gameDisplay.blit(heromg, (hx,hy))
score = [0, 0]
def game_loop():
    x = 1000
    y = 870
    hx = 10
    hy = 0
    sx = 0
    sy = 960
    #apart from start the next first plain rectangle
    px = 0
    py = 790
    #second plain reectangle
    p2y = 600
    #third plain rectangle
    p3y = 410
    #fourth plain rectangle
    p4y = 210
    #end one
    pend = 0
    #in first plain rectangle
            #first obstacle 
    mx = 320
    my = 780
    #in second plain rectangle
            #first obstacle
    m2x = 720
    m2y = 590
    g1x = 90
    g1y = 590
    #mov obs
    movx = -5
    movy = 260
    x_change = 0
    y_change = 0
    movsp = 2
    movsp2 =2
    leadrect.topleft = (x,y)
    mullarect.topleft = (mx,my)
    mulla2rect.topleft = (m2x,m2y)
    ghostrect.topleft = (g1x,g1y)
    shiprect.topleft = (movx,movy)
    gameDisplay.fill(black)
#    minl(x,y)
    clock = pygame.time.Clock()
    gamexit = False

    while not gamexit:

        gameDisplay.fill(black)
        start(sx,sy)
        plaen(px,py)
        palen2(px,p2y)
        palen3(px,p3y)
        palen4(px,p4y)
        palend(px,pend)
        lead(x,y)
        hero(hx,hy)
        mulla(mx,my)
        mulla2(m2x,m2y)
        ghost(g1x,g1y)
        ship(movx,movy)
        movx +=movsp
        text=font.render("score",True,Blue,white)
        text1=font.render(str(score[0]),True,Blue,white)
        textRect = text.get_rect()
        text1Rect = text1.get_rect()
        textRect.center =(250,20)
        text1Rect.center = (340,20)
        leadrect.topleft = (x,y)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gamexit = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    x_change = -7
                    #pygame.transform.rotate(lead, 180)
                if event.key == pygame.K_w:
                    y_change = -7
                if event.key == pygame.K_d:
                    x_change = 7
                if event.key == pygame.K_x:
                    y_change = 7
                    
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_a or event.key == pygame.K_w or event.key == pygame.K_d or event.key == pygame.K_x:
                    x_change = 0
                    y_change = 0
        #print(x)
       # print (y)
        x +=x_change
        y +=y_change
        if (y < 780 and y > 590-75):
         score[0] = 5
        if (y < 780 and y >260-75):
         score[0] = 15
        if (y < 780 and y > 50):
         score[0] = 25
        gameDisplay.blit(text,textRect)
        gameDisplay.blit(text1,text1Rect)
        #boundaries
        #print(display_height-130)
        #if y < my and x >= 0.08 and x <= 0.10456:
         #   gamexit = True
        if x > display_width -80 or x < 0:
            gamexit = True
        if  y < 0:
            x = 1000
            y = 870
            movsp+=1
            game2_loop()
        if y > 1200:
            gamexit = True
            
        #if y < my+80 and ( x < mx+80 or x > mx) :
         #   print ('yes')
       # if x > mx and x < mx+80 or x+100 > mx and x + 100 < mx+80:
        #     print ('collision')

        # For collision detection
        offs_x,offs_y = (mullarect.left - leadrect.left), (mullarect.top - leadrect.top)
        #second obst
        offsec_x,offsec_y = (mulla2rect.left - leadrect.left), (mulla2rect.top - leadrect.top)
        #second first ghost
        offg1_x,offg1_y = (ghostrect.left - leadrect.left), (ghostrect.top - leadrect.top)
        #movingobstacle
        #offsp_x,offsp_y = (shiprect.left - leadrect.left), (shiprect.top - leadrect.top)
        if (leadmg_mask.overlap(mullamg_mask, (offs_x, offs_y))!= None):
            if score[0] >= 5 :
                    score[0]=score[0]-5
            elif score[0] < 5:
                    gamexit = True
        if (leadmg_mask.overlap(mullamg_mask, (offsec_x, offsec_y))!= None):
            if score[0] >= 5 :
                    score[0]=score[0]-5
            if score[0] < 5:
                    gamexit = True
        if (leadmg_mask.overlap(ghostmg_mask, (offg1_x, offg1_y))!= None):
            if score[0] >= 5 :
                    score[0]=score[0]-5
            if score[0] < 5:
                    gamexit = True
        #if (y < movy+30):
         #   print('collison hehe he !!!!')
        if x>movx-80 and x < movx+80 and y> movy-80 and y< movy+80:
            if score[0] >= 10 :
                    score[0]=score[0]-10
            if score[0] < 10:
                    gamexit = True
        if movx > display_width+50-80:
            movx=display_width*0
        pygame.display.update()
        clock.tick(60)
        ticks = pygame.time.get_ticks()
        #print (ticks)

#******************************(((((((((((((((((((((((((((((((((((((((((*****************************************************))))))))))))))))))))))))))))))***)))****************^^^^^^^^^^^^
def game2_loop():
    hx = (display_width * 0)
    hy = (display_height * 0)
    sx = (display_width * 0)
    sy = (display_height *0.943 )
    #apart from start the next first plain rectangle
    px = (display_width * 0)
    py = (display_height *0.75)
    #second plain reectangle
    p2y = (display_height * 0.55)
    #third plain rectangle
    p3y = (display_height * 0.35)
    #fourth plain rectangle
    p4y = (display_height * 0.15)
    #end one
    pend = (display_height * -0.02)
    #in first plain rectangle
            #first obstacle 
    mx = (display_width * 0.3)
    my = (display_height *0.73)
    #in second plain rectangle
            #first obstacle
    m2x = (display_width *0.5)
    m2y = (display_height * 0.53)
    g1x = (display_width *0.1)
    g1y = (display_height * 0.53)
    #mov obs
    movx2 = (display_width * 0)
    movy2 = (display_height * 0.2)
    x2_change = 0
    y2_change = 0
    #leadrect.topleft = (x,y)
    mullarect.topleft = (mx,my)
    mulla2rect.topleft = (m2x,m2y)
    ghostrect.topleft = (g1x,g1y)
    shiprect.topleft = (movx2,movy2)
    movsp2 = 2
    gameDisplay.fill(black)
    gamexit = False
    while not gamexit:

        gameDisplay.fill(black)
        start(sx,sy)
        plaen(px,py)
        palen2(px,p2y)
        palen3(px,p3y)
        palen4(px,p4y)
        palend(px,pend)
        #lead(x,y)
        hero(hx,hy)
        mulla(mx,my)
        mulla2(m2x,m2y)
        ghost(g1x,g1y)
        ship(movx2,movy2)
        movx2 +=movsp2
        text2=font.render("score2",True,Blue,white)
        text12=font.render(str(score[1]),True,Blue,white)
        text2Rect = text2.get_rect()
        text12Rect = text12.get_rect()
        text2Rect.center =(550,20)
        text12Rect.center = (670,20)
        herorect.topleft = (hx,hy)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gamexit = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x2_change = -5
                    #pygame.transform.rotate(lead, 180)
                if event.key == pygame.K_UP:
                    y2_change = -5
                if event.key == pygame.K_RIGHT:
                    x2_change = 5
                if event.key == pygame.K_DOWN:
                    y2_change = 5
            if event.type == pygame.KEYUP:
               if event.key == pygame.K_LEFT or event.key == pygame.K_UP or event.key == pygame.K_RIGHT or event.key == pygame.K_DOWN:
                    x2_change = 0
                    y2_change = 0
        hx +=x2_change
        hy +=y2_change
        if (hy > 50 and hy < 590-75):
         score[1] = 5
        if (hy > 780 and hy >260-75):
         score[1] = 15
        if (hy < 780 and hy > 50):
         score[1] = 25
        gameDisplay.blit(text2,text2Rect)
        gameDisplay.blit(text12,text12Rect)
        if hx > display_width -80 or hx < 0:
            gamexit = True
        if  hy > display_height-80:
            hx = (display_width * 0.1)
            hy = (display_height *0.1)
            movsp2+=1
            #game2_loop()
        if hy < 0:
            if score[1] > score[0]:
                gameDisplay.fill(black)
                textwinner = font.render("Winner  ---> PLAYER1",True,Blue,white)
                textwinnerRect = textwinner.get_rect()
                textwinnerRect.center = (550,20)

            gamexit = True
        offsh_x,offsh_y = (mullarect.left - herorect.left), (mullarect.top - herorect.top)
        #second obst
        offshec_x,offshec_y = (mulla2rect.left - herorect.left), (mulla2rect.top - herorect.top)
        #second first ghost
        offgh1_x,offgh1_y = (ghostrect.left - herorect.left), (ghostrect.top - herorect.top)
        #movingobstacle
        #offsp_x,offsp_y = (shiprect.left - leadrect.left), (shiprect.top - leadrect.top)
        if (heromg_mask.overlap(mullamg_mask, (offsh_x, offsh_y))!= None):
            player2score=score[1]
            score[1]=0
            gamexit = True
           # if score[1] >= 5 :
            #        score[1]=score[1]-5
             #   elif score[1] < 5:
        if (heromg_mask.overlap(mullamg_mask, (offshec_x, offshec_y))!= None):
            player2score=score[1]
            score[1]=0
            gamexit = True
            #if score[1] >= 5 :
              #      score[1]=score[1]-5
             #   elif score[1] < 5:
                    
        if (heromg_mask.overlap(ghostmg_mask, (offgh1_x, offgh1_y))!= None):
            #if score[1] >= 5 :
             #  elif score[1] < 5:
             player2score=score[1]
             score[1]=0
             gamexit = True
        #if (y < movy+30):
         #   print('collison hehe he !!!!')
        if hx>movx2-80 and hx < movx2 +80 and hy > movy2 -80 and hy < movy2 +80:
            #if score[1] >= 10 :
                 #   score[1]=score[1]-10
                #elif score[1] < 10:
                    player2score=score[1]
                    score[1]=0
                    gamexit = True
        if movx2 > display_width+50-80:
            movx2 = display_width*0
        pygame.display.update()
        clock.tick(60)


game_loop()
pygame.quit()
quit()
